package es.grappl.cinematch.app.util

import android.content.res.Resources
import android.graphics.Bitmap
import android.graphics.Canvas
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import es.grappl.cinematch.app.AppContext

/**
 * User: elijah
 * Date: 01/04/15
 */
object UIUtils {

    fun takeScreenshot(v: View): Bitmap {
        var b = Bitmap.createBitmap(v.getWidth(), v.getHeight(), Bitmap.Config.ARGB_8888)
        var c = Canvas(b)
        v.draw(c)
        return b
    }

    fun showToast(text: String) {
        Toast.makeText(AppContext.context, text, Toast.LENGTH_LONG).show()
    }

    fun showToast(resourceId: Int) {
        showToast(AppContext.context.getResources().getString(resourceId))
    }

    fun measureViewDimensions(view: View): Pair<Int, Int> {
        var widthMeasureSpec = View.MeasureSpec.makeMeasureSpec(0, View.MeasureSpec.UNSPECIFIED)
        var heightMeasureSpec = View.MeasureSpec.makeMeasureSpec(0, View.MeasureSpec.UNSPECIFIED)
        view.measure(widthMeasureSpec, heightMeasureSpec)
        return Pair(view.getMeasuredWidth(), view.getMeasuredHeight())
    }

    fun getScaledDimension(original: Float): Float {
        var dm = Resources.getSystem().getDisplayMetrics()
        return original * dm.scaledDensity
    }

}

fun View.removeFromParent() {
    var parent = getParent()

    if (parent != null && parent is ViewGroup) {
        (parent as ViewGroup).removeView(this)
    }
}

fun View.setViewLoading(state: Boolean) {
    this.setEnabled(!state)
}