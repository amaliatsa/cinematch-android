package es.grappl.cinematch.app.core

import es.grappl.cinematch.app.model.AuthToken
import es.grappl.cinematch.app.model.Movie
import es.grappl.cinematch.app.model.TmdbResponse
import es.grappl.cinematch.app.network.API
import es.grappl.cinematch.app.network.Handler
import es.grappl.cinematch.app.network.StatusResponseHandler
import java.util.ArrayList

/**
 * User: elijah
 * Date: 17/02/15
 */

object CMController {

    var auth: AuthToken? = null
    var popularMovies = ArrayList<Movie>()

    fun getPopularMovies(callback: () -> Unit) {
        API.apiClient.getPopularMovies(response = Handler<TmdbResponse>(
                onSuccess = { status, payload ->
                    popularMovies.clear()
                    popularMovies.addAll(payload.results)

                    callback()
                }
        ))
    }

    fun likeMovie(movieId: Int, callback: () -> Unit) {
        API.authorize { token ->
            API.apiClient.likeMovie(token, movieId, StatusResponseHandler(
                    onSuccess = { status ->
                        callback()
                    }
            ))
        }
    }

    fun dislikeMovie(movieId: Int, callback: () -> Unit) {
        API.authorize { token ->
            API.apiClient.dislikeMovie(token, movieId, StatusResponseHandler(
                    onSuccess = { status ->
                        callback()
                    }
            ))
        }
    }

    fun addMovieToWatchlist(movieId: Int, callback: () -> Unit) {
        API.authorize { token ->
            API.apiClient.addToWatchlist(token, movieId, StatusResponseHandler(
                    onSuccess = { status ->
                        callback()
                    }
            ))
        }
    }

}