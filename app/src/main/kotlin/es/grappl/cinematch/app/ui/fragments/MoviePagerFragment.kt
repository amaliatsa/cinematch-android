package es.grappl.cinematch.app.ui.fragments

import android.view.View
import android.widget.ImageView
import android.widget.TextView
import com.squareup.picasso.Picasso
import es.grappl.cinematch.R
import es.grappl.cinematch.app.AppContext
import es.grappl.cinematch.app.core.CMController
import es.grappl.cinematch.app.model.Movie
import kotlin.properties.Delegates

/**
 * User: elijah
 * Date: 17/02/15
 */

public class MoviePagerFragment : CMFragment(R.layout.fragment_movie_pager, R.string.app_name) {

    var movieTitleView: TextView by Delegates.notNull()
    var moviePosterView: ImageView by Delegates.notNull()

    override fun customizeView(view: View) {
        movieTitleView = view.findViewById(R.id.movie_title) as TextView
        moviePosterView = view.findViewById(R.id.movie_poster) as ImageView

        view.findViewById(R.id.skip_button).setOnClickListener { view ->
            showNext()
        }

        view.findViewById(R.id.add_to_queue_button).setOnClickListener { view ->
            if (currentMovie != null) {
                CMController.addMovieToWatchlist(currentMovie!!.id) {
                    showNext()
                }
            }
        }

        view.findViewById(R.id.like_button).setOnClickListener { view ->
            if (currentMovie != null) {
                CMController.likeMovie(currentMovie!!.id) {
                    showNext()
                }
            }
        }

        view.findViewById(R.id.dislike_button).setOnClickListener { view ->
            if (currentMovie != null) {
                CMController.dislikeMovie(currentMovie!!.id) {
                    showNext()
                }
            }
        }
    }

    override fun initData() {
        CMController.getPopularMovies {
            showNext()
        }
    }

    var currentMovie: Movie? = null

    fun showNext() {
        if (!CMController.popularMovies.isEmpty()) {
            currentMovie = CMController.popularMovies.remove(0)

            movieTitleView.setText(currentMovie!!.original_title)

            Picasso.with(AppContext.context)
                    .load(currentMovie!!.poster_path)
                    .into(moviePosterView)
        }
    }
}
