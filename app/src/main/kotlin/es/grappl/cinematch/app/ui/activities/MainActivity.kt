package es.grappl.cinematch.app.ui.activities

import es.grappl.cinematch.app.ui.fragments.MoviePagerFragment
import es.grappl.cinematch.app.ui.fragments.SplashFragment

public class MainActivity : CMActivity(SplashFragment()), SplashFragment.SessionStartListener {

    override fun sessionStarted() {
        setMainFragment(MoviePagerFragment(), false)
    }


}