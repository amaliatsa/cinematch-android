package es.grappl.cinematch.app.network

import android.util.Log
import com.google.gson.GsonBuilder
import com.google.gson.JsonDeserializationContext
import com.google.gson.JsonDeserializer
import com.google.gson.JsonElement
import es.grappl.cinematch.R
import es.grappl.cinematch.app
import es.grappl.cinematch.app.core.Keys
import es.grappl.cinematch.app.core.CMController
import es.grappl.cinematch.app.core.Storage
import es.grappl.cinematch.app.model.AuthToken
import es.grappl.cinematch.app.model.CMResponse
import es.grappl.cinematch.app.model.CMStatusResponse
import es.grappl.cinematch.app.model.ResponseStatus
import es.grappl.cinematch.app.util.Device
import es.grappl.cinematch.app.util.UIUtils
import retrofit.Callback
import retrofit.RestAdapter
import retrofit.RetrofitError
import retrofit.client.Response
import retrofit.converter.GsonConverter
import java.lang.reflect.Type
import java.util.Date

/**
 * User: elijah
 * Date: 17/02/15
 */

object API {

    val TAG = "API"

    val gson = GsonBuilder()
            .registerTypeAdapter(javaClass<Date>(), object : JsonDeserializer<Date> {
                override fun deserialize(json: JsonElement?, expectedType: Type?, context: JsonDeserializationContext?): Date? {
                    return Date(json!!.getAsLong())
                }
            })
            .create()

    var apiClient = RestAdapter.Builder()
            .setEndpoint(app.AppContext.context.getString(R.string.server_url))
            .setConverter(GsonConverter(gson))
            .build()
            .create(javaClass<CMService>())

    fun authorize(next: ((String) -> Unit)) {
        if (CMController.auth != null) {
            next(CMController.auth!!.token)
        } else {
            var token = Storage.readPreference(Keys.API_TOKEN)
            if (token != null) {
                next(token!!)
            } else {
                println("Attempted to invoke a protected API without a token")
            }
        }
    }

    fun startSession(retryOnFail: Boolean = false, next: ((String) -> Unit)? = null) {
        apiClient.getToken(Device.getID(), Handler<AuthToken>(
                onSuccess = { status, payload ->
                    CMController.auth = payload
                    Storage.writePreference(Keys.API_TOKEN, payload.token)

                    if (next != null) {
                        next(payload.token)
                    }
                },
                onFailure = { error ->
                    if (retryOnFail) {
                        startSession(retryOnFail, next)
                    } else {
                        StatusResponseHandler().failure(error)
                    }
                }
        ))
    }
}

open class Handler<S>(val onSuccess: (ResponseStatus, S) -> Unit, val onFailure: ((RetrofitError) -> Unit)? = null) : Callback<CMResponse<S>> {
    override fun success(t: CMResponse<S>?, response: Response?) {
        onSuccess(t!!.status, t.payload)
    }

    override fun failure(error: RetrofitError?) {
        if (onFailure != null) {
            onFailure!!(error!!)
        } else {
            StatusResponseHandler().failure(error)
        }
    }
}

open class StatusResponseHandler(val onSuccess: ((ResponseStatus) -> Unit)? = null) : Callback<CMStatusResponse> {
    override fun success(t: CMStatusResponse?, response: Response?) {
        if (onSuccess != null) {
            onSuccess!!(t!!.status)
        } else {
            UIUtils.showToast(if (t?.error != null) t?.error.toString() else t?.status.toString())
        }
    }

    override fun failure(error: RetrofitError?) {
        Log.e(API.TAG, error.toString())
        UIUtils.showToast(error.toString())
    }
}
