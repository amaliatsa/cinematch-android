package es.grappl.cinematch.app.network;

import es.grappl.cinematch.app.model.*
import retrofit.Callback
import retrofit.http.GET
import retrofit.http.Query

/**
 * User: elijah
 * Date: 17/02/15
 */
public trait CMService {

    [GET("/auth/token")]
    fun getToken([Query("deviceId")] deviceId: String,
                 response: Callback<CMResponse<AuthToken>>)

    [GET("/movies/search")]
    fun searchMovies([Query("title")] title: String,
                     [Query("page")] page: Int? = null,
                     response: Callback<CMResponse<TmdbResponse>>)

    [GET("/movies/popular")]
    fun getPopularMovies([Query("page")] page: Int? = null,
                         response: Callback<CMResponse<TmdbResponse>>)

    [GET("/ratings")]
    fun getRatings([Query("token")] token: String,
                   response: Callback<CMResponse<List<Rating>>>)

    [GET("/ratings/like")]
    fun likeMovie([Query("token")] token: String,
                  [Query("movieId")] movieId: Int,
                  response: Callback<CMStatusResponse>)

    [GET("/ratings/dislike")]
    fun dislikeMovie([Query("token")] token: String,
                     [Query("movieId")] movieId: Int,
                     response: Callback<CMStatusResponse>)

    [GET("/watchlist")]
    fun getWatchlist([Query("token")] token: String,
                     response: Callback<CMResponse<List<WatchlistItem>>>)

    [GET("/watchlist/add")]
    fun addToWatchlist([Query("token")] token: String,
                       [Query("movieId")] movieId: Int,
                       response: Callback<CMStatusResponse>)

    [GET("/watchlist/remove")]
    fun removeFromWatchlist([Query("token")] token: String,
                            [Query("movieId")] movieId: Int,
                            response: Callback<CMStatusResponse>)

}
