package es.grappl.cinematch.app.ui.activities

import android.os.Bundle
import android.support.v4.app.FragmentActivity
import android.view.View
import android.widget.LinearLayout
import android.widget.TextView
import es.grappl.cinematch.R
import es.grappl.cinematch.app.ui.fragments.CMFragment
import kotlin.properties.Delegates

/**
 * User: elijah
 * Date: 17/02/15
 */
abstract public class CMActivity(val defaultFragment: CMFragment? = null) : FragmentActivity() {

    var actionBar: LinearLayout by Delegates.notNull()
    var actionBarTitle: TextView by Delegates.notNull()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        setContentView(R.layout.activity_container)

        actionBar = findViewById(R.id.action_bar) as LinearLayout
        actionBarTitle = findViewById(R.id.title) as TextView

        if (savedInstanceState == null && defaultFragment != null) {
            setMainFragment(defaultFragment, false)
        }
    }

    var mainFragment: CMFragment by Delegates.notNull()

    fun setMainFragment(fragment: CMFragment, addToBackStack: Boolean = true) {
        mainFragment = fragment

        var transaction = getSupportFragmentManager().beginTransaction().replace(R.id.container, fragment)
        if (addToBackStack) {
            transaction.addToBackStack(null)
        }
        transaction.commit()
    }

    override fun onStart() {
        super.onStart()

        if (mainFragment.actionBarTitle != null) {
            actionBar.setVisibility(View.VISIBLE)
            setActionBarTitle(getString(mainFragment.actionBarTitle!!))
        } else {
            actionBar.setVisibility(View.GONE)
        }
    }

    fun popMainFragment() {
        getSupportFragmentManager().popBackStack()
    }

    fun setActionBarTitle(title: String) {
        actionBarTitle.setText(title)
    }

}