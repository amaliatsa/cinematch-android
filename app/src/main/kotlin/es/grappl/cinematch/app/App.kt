package es.grappl.cinematch.app

import android.app.Application
import android.content.Context
import android.hardware.SensorManager
import android.view.LayoutInflater
import es.grappl.cinematch.app.core.CMController
import es.grappl.cinematch.app.network.API
import es.grappl.cinematch.app.network.Handler
import kotlin.properties.Delegates

/**
 * User: elijah
 * Date: 17/02/15
 */
public class App() : Application() {

    override fun onCreate() {
        AppContext.init(getApplicationContext())

        initData()
    }

    fun initData() {

    }
}

object AppContext {

    var context: Context by Delegates.notNull()
    var inflater: LayoutInflater by Delegates.notNull()
    var sensorManager: SensorManager by Delegates.notNull()

    fun init(context: Context) {
        this.context = context
        this.inflater = context.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
        this.sensorManager = context.getSystemService(Context.SENSOR_SERVICE) as SensorManager
    }
}