package es.grappl.cinematch.app.ui.fragments

import android.app.Activity
import android.view.View
import es.grappl.cinematch.R
import es.grappl.cinematch.app.network.API
import kotlin.concurrent.thread

/**
 * User: elijah
 * Date: 08/04/15
 */
public class SplashFragment : CMFragment(R.layout.fragment_splash) {

    trait SessionStartListener {
        fun sessionStarted()
    }

    override fun onAttach(activity: Activity) {
        super.onAttach(activity)

        if (activity !is SessionStartListener) {
            throw IllegalStateException("Attach SplashFragment only to activities implementing the SessionStartListener trait")
        }
    }

    override fun customizeView(view: View) {
    }

    override fun initData() {
        thread() {
            Thread.sleep(2000)

            API.startSession(true) { token ->
                (getActivity() as SessionStartListener).sessionStarted()
            }
        }
    }

}