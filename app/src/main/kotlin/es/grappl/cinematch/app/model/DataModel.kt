package es.grappl.cinematch.app.model

/**
 * User: elijah
 * Date: 17/02/15
 */

// generic

enum class ResponseStatus {
    OK
    FAILURE
}

data class CMResponse<T>(
        val status: ResponseStatus,
        val payload: T,
        val error: String?
)

data class CMStatusResponse(
        val status: ResponseStatus,
        val error: String?
)

// auth

data class AuthToken(
        val token: String
)

// movies
data class TmdbResponse(
        val page: Int,
        val results: List<Movie>,
        val total_pages: Int,
        val total_results: Int
)

data class Movie(
        val adult: Boolean,
        val backdrop_path: String,
        val id: Int,
        val original_title: String,
        val release_date: String,
        val poster_path: String,
        val popularity: Float,
        val title: String,
        val video: Boolean,
        val vote_average: Float,
        val vote_count: Int
)

// ratings
data class Rating(
        val movieId: String,
        val rating: Int,
        val addedOn: String
)

// watchlist
data class WatchlistItem(
        val movieId: String,
        val addedOn: String
)