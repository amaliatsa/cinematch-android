package es.grappl.cinematch.app.util

import android.util.Base64
import es.grappl.cinematch.app.AppContext
import java.math.BigInteger
import java.security.MessageDigest

/**
 * User: elijah
 * Date: 21/02/15
 */
object Utils {

    fun getResourceByName(name: String, type: String): Int {
        return AppContext.context.getResources().getIdentifier(name, type, AppContext.context.getPackageName())
    }

    fun getStaticMapURL(hint: String, viewWidth: Int, viewHeight: Int): String {
        return "https://maps.googleapis.com/maps/api/staticmap?center=${hint}&zoom=13&size=${viewWidth}x${viewHeight}&style=feature:all|element:geometry|color:0xD2D2D2&style=feature:landscape.man_made|element:geometry|color:0xEFEFEF&style=feature:road|element:geometry|color:0xFFFFFF"
    }
}

fun String.hash(): String {
    var md = MessageDigest.getInstance("MD5")

    return BigInteger(1, md.digest(this.toByteArray("UTF8"))).toString(16)
}

fun String.toBase64(): String {
    return Base64.encodeToString(this.toByteArray("UTF8"), Base64.DEFAULT)
}

fun String.fromBase64(): String {
    return String(Base64.decode(this, Base64.DEFAULT))
}