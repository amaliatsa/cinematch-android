package es.grappl.cinematch.app.ui.fragments

import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup

/**
 * User: elijah
 * Date: 11/03/15
 */
public abstract class CMFragment(val layoutResource: Int,
                                 val actionBarTitle: Int? = null) : Fragment() {

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View {
        val view = inflater.inflate(layoutResource, container, false)

        customizeView(view)

        return view
    }

    override fun onResume() {
        super.onResume()

        initData()
    }

    abstract fun customizeView(view: View)

    abstract fun initData()
}