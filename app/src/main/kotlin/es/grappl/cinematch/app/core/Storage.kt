package es.grappl.cinematch.app.core

import android.content.SharedPreferences
import android.preference.PreferenceManager
import es.grappl.cinematch.app
import es.grappl.cinematch.app.AppContext

/**
 * User: elijah
 * Date: 12/03/15
 */
object Storage {

    fun getPreferences(): SharedPreferences {
        return PreferenceManager.getDefaultSharedPreferences(app.AppContext.context)
    }

    fun getPreferencesEditor(): SharedPreferences.Editor {
        return getPreferences().edit()
    }

    fun writePreference(key: String, value: String) {
        var editor = getPreferencesEditor()
        editor.putString(key, value)
        editor.apply()
    }

    fun readPreference(key: String, defaultValue: String? = null): String? {
        return getPreferences().getString(key, defaultValue)
    }
}

object Keys {
    val API_TOKEN = "api.token"
}