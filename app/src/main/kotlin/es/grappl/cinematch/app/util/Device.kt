package es.grappl.cinematch.app.util

import android.content.Context
import android.os.Vibrator
import android.provider.Settings
import es.grappl.cinematch.app.AppContext

/**
 * User: elijah
 * Date: 21/02/15
 */
object Device {

    fun getID(): String {
        return Settings.Secure.getString(AppContext.context.getContentResolver(), Settings.Secure.ANDROID_ID).hash()
    }

    fun hapticFeedback() {
        var vibrator = AppContext.context.getSystemService(Context.VIBRATOR_SERVICE) as Vibrator
        vibrator.vibrate(100)
    }

}